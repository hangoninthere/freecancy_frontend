import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Table from 'components/Table.vue'

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Table',
      component: Table
    }
  ]
})
